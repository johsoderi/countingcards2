#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os, sys
import subprocess
import random
import time
import datetime
import platform
# Recommended terminal settings: Size: 40*31, Font: Speccy Medium 24p, Foreground: White, Background: Black

# Settings:
baseDifficulty = 0.5    # Time in seconds, higher is easier. (baseDifficulty / difficulty) * 34 gives total counting time. Default: 0.5
sweepTime = 0.04        # Make this a lower number for faster cutscenes. Default: 0.05
margin1 = (" ")         # Narrow left margin.
# Game time at different baseDifficulty:
# baseDifficulty 0.3: [difficulty 1: 10.20sec.]  [difficulty 5: 2.04sec.]  [difficulty 9: 1.13sec.]
# baseDifficulty 0.5: [difficulty 1: 17.00sec.]  [difficulty 5: 3.40sec.]  [difficulty 9: 1.89sec.]
# baseDifficulty 1.0: [difficulty 1: 34.00sec.]  [difficulty 5: 6.80sec.]  [difficulty 9: 3.78sec.]
# baseDifficulty 2.0: [difficulty 1: 68.00sec.]  [difficulty 5: 13.60sec.] [difficulty 9: 7.56sec.]

#----/ Formatting /---------------------------------------------------------------------------#
def senBlirAlltSvart():                                                   # Clears the screen
    if (os.name == "posix"): os.system("clear")
    elif (platform.system() == "Windows"): os.system("cls")
    else: pass
def ln (newLines=1):                                 # ln() prints 1 new line. ln(3) prints 3
    print(f"\n" * newLines, end = "")
def mv(row, col):                                                          # Move text cursor
    print(f"\33[{row};{col}H",end="")
    return ""
# Below are a few escape sequences to change colors and to show unicode characters:
# "\33" is the escape character.
# "[31m" and "[30m" makes the text red or black respectively. "[107m" makes background white.
# "[0m" resets
# u'\u2665' is the unicode for "Black HEARTS Suit"
# u'\u2660' is the unicode for "Black SPADES Suit"
# u'\u2666' is the unicode for "Black DIAMONDS Suit"
# u'\u2666' is the unicode for "Black CLUBS Suit"
e = '\33['
fBlk, fRed, fGrn, fYel, fBlu, fMag, fCya, fWht = \
e+'30m',e+'31m',e+'32m',e+'33m',e+'34m',e+'35m',e+'36m',e+'37m'                  # Text color
bBlk, bRed, bGrn, bYel, bBlu, bMag, bCya, bWht = \
e+'40m',e+'41m',e+'42m',e+'43m',e+'44m',e+'45m',e+'46m',e+'47m'            # Background color
bold, nobold, ital, noital, under, nounder, blink, noblink = \
e+'1m',e+'22m', e+'3m', e+'23m', e+'4m',e+'24m', e+'5m', e+'25m'                 # Text style
allOff = e+'0m'                                                         # Reset color & style

# Various characters:
heartSuit = "\33[31m\33[107m\u2665" + allOff
spadeSuit = "\33[30m\33[107m\u2660" + allOff
diamondSuit = "\33[31m\33[107m\u2666" + allOff
clubSuit = "\33[30m\33[107m\u2663" + allOff
securityFilled = fYel + "\u25c6" + allOff
securityEmpty = fYel + "\u25c7" + allOff

# Various variables:
theCount = 0
totalCashEarned = 0
colors = ["HEARTS", "SPADES", "DIAMONDS", "CLUBS"]
try:
    difficulty = int(sys.argv[1]) - 1 # Choose level to start at
except:
    difficulty = 0 # Current level, change baseDifficulty to alter actual difficulty
missionsPath = './cc_missions.txt'
missions = [line.rstrip() for line in open(missionsPath)]

def drawBox(**kwargs):
    fg = kwargs["fg"]
    bg = kwargs["bg"]
    innerW = kwargs["innerW"]
    section1 = kwargs["section1"]
    section2 = kwargs["section2"]
    section3 = kwargs["section3"]
    section4 = kwargs["section4"]
    print(f"{fg+bg}", end="") # Foreground/Background color
    print(f"\u2554", end="") # (u"\u2554") + ╔
    for x in range (innerW):
        print(f"\u2550", end="") # (u"\u2550") + ═
    print(f"\u2557", end="\n") # (u"\u2557") + ╗
    for x in range (section1):
        print(f"\u2551", end="") # (u"\u2551") + ║
        for x in range (innerW):
            print(f" ", end="")
        print(f"\u2551", end="\n") # (u"\u2551") + ║
    if section2 > 0:
        print(f"\u2560", end="") # (u"\u2560") + ╠
        for x in range (innerW):
            print(f"\u2550", end="") # (u"\u2550") + ═
        print(f"\u2563", end="\n") # (u"\u2563") + ╣
        for x in range (section2):
            print(f"\u2551", end="") # (u"\u2551") + ║
            for x in range (innerW):
                print(f" ", end="")
            print(f"\u2551", end="\n") # (u"\u2551") + ║
        if section3 > 0:
            print(f"\u2560", end="") # (u"\u2560") + ╠
            for x in range (innerW):
                print(f"\u2550", end="") # (u"\u2550") + ═
            print(f"\u2563", end="\n") # (u"\u2563") + ╣
            for x in range (section3):
                print(f"\u2551", end="") # (u"\u2551") + ║
                for x in range (innerW):
                    print(f" ", end="")
                print(f"\u2551", end="\n") # (u"\u2551") + ║
            if section4 > 0:
                print(f"\u2560", end="") # (u"\u2560") + ╠
                for x in range (innerW):
                    print(f"\u2550", end="") # (u"\u2550") + ═
                print(f"\u2563", end="\n") # (u"\u2563") + ╣
                for x in range (section4):
                    print(f"\u2551", end="")
                    for x in range (innerW):
                        print(f" ", end="")
                    print(f"\u2551", end="\n")
    print(f"\u255A", end="")# (u"\u255A") + ╚
    for x in range (innerW):
        print(f"\u2550", end="") # (u"\u2550") + ═
    print(f"\u255D", end="") # (u"\u255D") + ╝
    print(f"{allOff}",end="")

def intro():
    introTxt = ["" for i in range(7)]
    introTxt[0] = " FOUR WEEKS AGO, YOU WERE LIVING A\n\n" \
                  " HAPPY LIFE AS A PROBABILITY SCIENTIST\n\n" \
                  " WITH SAVANT SYNDROME."
    introTxt[1] = " THEN ONE NIGHT,\n\n" \
                  " YOU HAD A FEW PINTS TOO MANY AND YOU\n\n" \
                  " TOOK A BET INVOLVING SOME COINS,"
    introTxt[2] = " A\n\n PICKLE JAR,"
    introTxt[3] = " AND YOUR TEENAGE DAUGHTER.\n\n"
    introTxt[4] = " YOU LOST THE BET, AND HAVE SINCE BEEN\n\n" \
                  " WORKING FOR THE MOB, COUNTING CARDS.\n\n"
    introTxt[5] = " HOPING TO SEE YOUR BABY GIRL AGAIN, \n\n" \
                  " YOU HAVE AGREED TO USE YOUR SKILLS AT\n\n" \
                  " THE MOST EXCLUSIVE CASINOS AROUND THE\n\n" \
                  " WORLD."
    introTxt[6] = " FOR YOUR FIRST ASSIGNMENT,\n\n\n PRESS ENTER. "
    os.system('clear')
    print("\n")
    for i in range(len(introTxt)):
        for x in range(len(introTxt[i])):
            print(introTxt[i][x], end="")
            sys.stdout.flush()
            time.sleep(sweepTime/3) #0.015
        time.sleep(sweepTime*10) # 0.3
    input("")
    missionScreen()

def failure():
    nextLoc = missions[((difficulty+1)*14)-12]
    deathText = ["" for i in range(8)]
    deathText[0] = " AFTER A LONG AND UNCOMFORTABLE FLIGHT\n\n" \
                   " TO " + nextLoc + ", YOU REACH FOR YOUR BAG\n\n" \
                   " AT THE BAGGAGE CAROUSEL."
    deathText[1] = " YOU NOTICE\n\n THAT IT LOOKS STRANGELY" \
                   " DEFLATED,\n\n AND JUST TO MAKE SURE THAT EVERYTHING\n\n" \
                   " IS IN ORDER, YOU OPEN THE ZIPPER.\n\n"
    deathText[2] = " INSIDE THE BAG YOU FIND A HEAVY,\n\n" \
                   " OBLONG OBJECT WRAPPED IN PLASTIC.\n\n"
    deathText[3] = " ATTACHED TO THE PACKAGE IS A NOTE,\n\n" \
                   " AND AS YOU READ IT, YOU REALIZE WITH\n\n" \
                   " A SINKING HEART WHAT'S IN THE PACKAGE.\n\n"
    deathText[4] = " THE MESSAGE ON THE NOTE IS SHORT: \n\n"

    deathText[5] = " 'WE SAID $" + str(missionGoal) + ".'"
    deathText[6] = " IN THE PACKAGE IS\n\n THE HEAD OF YOUR" \
                   " BEAUTIFUL DAUGHTER.\n\n\n"
    deathText[7] = " PRESS ENTER TO SHOW YOUR SCORE"
    os.system('clear')
    print("\n")
    for i in range(len(deathText)):
        for x in range(len(deathText[i])):
            print(deathText[i][x], end="")
            sys.stdout.flush()
            time.sleep(sweepTime/3)
        time.sleep(sweepTime*10)
    input("")
    checkHighscore()


def win():
    winText = ["" for i in range(5)]
    winText[0] = " AS YOU LEAVE THE CASINO, YOU NOTICE\n\n" \
                 " THAT THE MAN MEETING YOU IS NOT THE\n\n" \
                 " USUAL DRIVER."
    winText[1] = " IT IS IN FACT NOT EVEN \n\n A MAN, IT'S YOUR DAUGHTER!!"
    winText[2] = " AS YOU\n\n EMBRACE, YOU BOTH START CRYING.\n\n"
    winText[3] = " THEN YOU BOOK THE FIRST FLIGHT HOME,\n\n" \
                 " GET MARRIED, HAVE LOTS OF DEFORMED\n\n" \
                 " CHILDREN AND LIVE HAPPILY EVER AFTER.\n\n\n"
    winText[4] = " PRESS ENTER TO SHOW YOUR SCORE"
    os.system('clear')
    print("\n")
    for i in range(len(winText)):
        for x in range(len(winText[i])):
            print(winText[i][x], end="")
            sys.stdout.flush()
            time.sleep(sweepTime/3)
        time.sleep(sweepTime*10)
    input("")
    checkHighscore()


def placeTheColor(color, level):
# This function randomly places the cards to count in the tableSpace list.
    global tableSpace
    tableSpace = ["freeSlot"] * 108 # This is a list for storing all the cards that will be placed on the "table"
    if level == 1: quantRange = [5, 10]
    if level == 2: quantRange = [8, 15]
    if level == 3: quantRange = [10, 18]
    if level == 4: quantRange = [12, 22]
    if level == 5: quantRange = [14, 26]
    if level == 6: quantRange = [16, 28]
    if level == 7: quantRange = [18, 30]
    if level == 8: quantRange = [20, 40]
    if level == 9: quantRange = [30, 58]
    global quantOfTheColor
    quantOfTheColor = (random.randrange(quantRange[0], quantRange[1])) # The quantity of cards to count ranges from 1-58.
    theColorLeft = quantOfTheColor
    while(theColorLeft > 0):
        trySlot = random.randrange(0, 107)
        if (tableSpace[trySlot] == "freeSlot"):
            tableSpace[trySlot] = theColor
            theColorLeft -= 1
    placeTheRest()


def placeTheRest():
# This function iterates through the tableSpace list and places a random card (theColor excepted) when it finds a free slot.
    restOfColors = []
    for color in colors:
        if color != theColor:
            restOfColors.append(color)
    wordNum = 0
    for word in tableSpace:
        if word == "freeSlot":
            tableSpace[wordNum] = random.choice(restOfColors)
        wordNum += 1
    dealTheCards()

####################################################################################################################################
def missionScreen():
    global difficulty
    global theColor
    global missionGoal
    theColor = random.choice(colors)
    difficulty +=1
    os.system('clear')
    margin3 = 38
    print("\033[31m", end="") # Background color
    print("\u2554", end="") # (u"\u2554") + ╔
    for x in range (margin3):
        print("\u2550", end="") # (u"\u2550") + ═
    print("\u2557", end="\n") # (u"\u2557") + ╗
    print("\u2551", end="") # (u"\u2551") + ║
    for x in range (38):
        print(" ", end="")
    print("\u2551", end="\n") # (u"\u2551") + ║
    print("\u2560", end="") # (u"\u2560") + ╠
    for x in range (margin3):
        print("\u2550", end="") # (u"\u2550") + ═
    print("\u2563", end="\n") # (u"\u2563") + ╣
    for x in range (9):
        print("\u2551", end="")
        for x in range (margin3):
            print(" ", end="")
        print("\u2551", end="\n")
    for x in range (8):
        print("\u2551", end="")
        for x in range (margin3):
            print(" ", end="")
        print("\u2551", end="\n")
    for x in range (2):
        print("\u2551", end="")
        for x in range (margin3):
            print(" ", end="")
        print("\u2551", end="\n")
    print("\u255A", end="")# (u"\u255A") + ╚
    for x in range (margin3):
        print("\u2550", end="") # (u"\u2550") + ═
    print("\u255D", end="") # (u"\u255D") + ╝

    missionGoal = missions[difficulty*14-11]
    print(f"{mv(2,2)}{fYel}{missions[(difficulty*14)-13].rstrip()}", end=allOff) # Casino name
    print(f"{mv(5,2)}{fRed}LOCATION:", end=allOff)
    print(f"{mv(7,2)}{fYel}{missions[(difficulty*14)-12]}", end=allOff)
    print(f"{mv(13,2)}{fRed}OBJECTIVES:", end=allOff)
    print(f"{mv(15,2)}{fYel}- COUNT ALL THE {theColor} ON THE TABLE", end=allOff)
    print(f"{mv(17,2)}{fYel}- EARN AT LEAST ${missionGoal}", end=allOff)
    print(f"{mv(9,2)}{fRed}SECURITY LEVEL:", end=allOff)
    print(f"{mv(11,2)}{securityFilled * difficulty}{securityEmpty * (9-difficulty)}", end=allOff)

    flagCode = compile("\n".join(missions[difficulty*14-10:difficulty*14+1]), "<string>", "exec")
    exec(flagCode)
    print(allOff)

    input("\033[31m\33[21;2H" + "PRESS ENTER TO BEGIN MISSION")
    placeTheColor(theColor, difficulty)

def dealTheCards():
# This function iterates through the tableSpace list and prints the correct unicodes to the screen.
    os.system('clear')
    # Draw the background:
    tableWidth = 38
    print("\033[35m", end="") # Background color
    print("\u2554", end="")
    for x in range (tableWidth):
        print("\u2550", end="")
    print("\u2557", end="\n")
    for x in range (19):
        print("\u2551", end="")
        for x in range (tableWidth):
            print(" ", end="")
        print("\u2551", end="\n")
    print("\u2560", end="")
    for x in range (tableWidth):
        print("\u2550", end="")
    print("\u2563", end="\n")
    print("\u2551", end="")
    for x in range (tableWidth):
        print(" ", end="")
    print("\u2551", end="\n")
    print("\u255A", end="")
    for x in range (tableWidth):
        print("\u2550", end="")
    print("\u255D", end="")
    print("\33[22A\33[2C", end="") # Move the cursor up 22 steps ('\33[22A'), and right 2 steps ('\33[2C')
    cardNumber = -1 # Have to offset this value to get even rows of cards
    for card in tableSpace:
        cardNumber += 1
        if cardNumber % 12 == 0: # When N number of cards have been printed, start a new line
            print("\n")
            print("\33[3C", end="") # Move cursor 3 steps right
        if(card == "HEARTS"): print(heartSuit, end="  ")
        if(card == "SPADES"): print(spadeSuit, end="  ")
        if(card == "DIAMONDS"): print(diamondSuit, end="  ")
        if(card == "CLUBS"): print(clubSuit, end="  ")
    theStress()

def theStress():
# ░░░░░░░░░▒▒▒▒▓▓▓▓▓▓▓▓░░░░▒▒▒▒▓▓▓▓▓▓▓▓▓
# This function prints the bar showing the time left.
    sleepTime = (baseDifficulty / difficulty) # If this code is used somewhere else, just set sleepTime to a value in seconds
    fg = ['\33[32m','\33[33m','\33[31m'] # Foreground: 0 32 Green, 1 33 Yellow, 2 31 Red
    bg = ['\33[42m','\33[43m','\33[41m'] # Background: Green, Yellow, Red
    ch = ['\u2591','\u2592','\u2593'] # Unicode symbols: ░,▒,▓
    print('\033[5 q', end="") # Set cursor style to thin line
    #print("\33[" + str(r) + ";" + str(c) + "H", end=" ")
    print("\33[22;2H", end="") # Move cursor to row 22, col 2.
    for x in range(1,39):
        if (x >= 1) and (x <= 5): print(fGrn + bGrn + ch[0], end=allOff)
        if (x >= 6) and (x <= 9): print(fYel + bGrn + ch[0], end=allOff)
        if (x >= 10) and (x <= 13): print(fYel + bGrn + ch[1], end=allOff)
        if (x >= 14) and (x <= 17): print(fYel + bGrn + ch[2], end=allOff)
        if (x >= 18) and (x <= 21): print(fYel + bYel + ch[2], end=allOff)
        if (x >= 22) and (x <= 25): print(fRed + bYel + ch[0], end=allOff)
        if (x >= 26) and (x <= 29): print(fRed + bYel + ch[1], end=allOff)
        if (x >= 30) and (x <= 33): print(fRed + bYel + ch[2], end=allOff)
        if (x >= 34) and (x <= 38): print(fRed + bRed + ch[2], end=allOff)
        sys.stdout.flush()
        time.sleep(sleepTime)
    for c in range(2, 20):
        for r in range(2,20): # Hide the table
            print("\33[" + str(r) + ";" + str(c) + "H", end=" ")
        for r in range(2,20): # Hide the table
            print("\33[" + str(r) + ";" + str(39-c) + "H", end=" ")
        sys.stdout.flush()
        time.sleep(0.03)
    print('\033[1 q', end="") # Reset cursor style
    theAnswer()

def theAnswer():
    global theCount
    haveInt = False
    while haveInt == False:
        try:
            print("\33[3;2H" + "ALRIGHT," + "\33[5;2H" + "HOW MANY " + theColor + " DID YOU SEE?") # cheat: + str(quantOfTheColor)
            theCount = int(input("\33[7;2H" + ">"))
            haveInt = True
        except ValueError:
            print("\33[8;2H" + "I ASKED FOR A NUMBER, YOU IMBECILE!\n\n")
            #sys.exit(1)
    theScore()


def theScore():
    global endScore
    global totalCashEarned
    # The score is calculated as follows:
    percentRight = (((quantOfTheColor - abs(quantOfTheColor-theCount)) / quantOfTheColor) * 100)
    if percentRight < 70:
        ratioScore = (percentRight * 0.5)
    if  percentRight >= 50:
        ratioScore = (percentRight * 5.5)
    if  percentRight >= 60:
        ratioScore = (percentRight * 6.0)
    if  percentRight >= 70:
        ratioScore = (percentRight * 6.5)
    if  percentRight >= 80:
        ratioScore = (percentRight * 7.0)
    if  percentRight >= 85:
        ratioScore = (percentRight * 7.5)
    if  percentRight >= 90:
        ratioScore = (percentRight * 8.0)
    if  percentRight >= 95:
        ratioScore = (percentRight * 9.0)
    if  percentRight == 100:
        ratioScore = (percentRight * 10.0)
    if quantOfTheColor < 10:
        newQuant = 10 # The ratioScore is the minumum endScore
    else:
        newQuant = quantOfTheColor
    endScore = int((ratioScore * (difficulty*30)) * (newQuant / 100)) # The game keeps the change
    totalCashEarned += endScore
    endScreen()

def checkHighscore():
    global highTotalCash
    os.system('clear')
    filename = './cc_highscore.txt'
    try:
        HSfile = open(filename,'r')
        highScore = HSfile.readlines()
        HSfile.close()
    except:
        newHighscore()
    try:
        highName = highScore[0].rstrip()
        highTotalCash = int(highScore[1].rstrip())
        highLevel = highScore[2].rstrip()
        highDate = highScore[3].rstrip()
    except:
        newHighscore()
    if (totalCashEarned > highTotalCash):
        newHighscore()
    else:
        print("\33[33;3H\33[31mYOUR SCORE: $" + str(totalCashEarned))
        print("\n  HIGHSCORE:  $" + str(highTotalCash))
        print("  NAME:       " + highName + "\n  LEVEL:      " + str(highLevel) + "\n  DATE:       " + highDate, end="\33[0m\n\n\n\n")
    sys.exit(0)


def newHighscore():
    print(" NEW HIGHSCORE!!!")
    sys.stdout.flush()
    time.sleep(2)
    os.system('clear')
    highestName = input(" CONGRATULATIONS,\n YOU MADE THE ALL-TIME HIGHSCORE!\n\n PLEASE TYPE YOUR NAME\n\n\n\n >")
    highestName = highestName.upper()
    today = datetime.date.today()
    filename = './cc_highscore.txt'
    HSfile = open(filename,'w')
    HSfile.write(highestName + "\n" + str(totalCashEarned) + "\n" + str(difficulty) + "\n" + str(today))
    HSfile.close()
    checkHighscore()


def endScreen():
# This function iterates slowly through the tableSpace list to print only the relevant cards, then prints the score.
    senBlirAlltSvart()
    theColorCount = 0 # For labeling the cards
    drawBox(fg=fGrn, bg=bBlk, innerW=38, section1=1, section2=19, section3=5, section4=1)
    print(f"{fYel}{mv(2,2)} NOW, LET'S SEE HOW YOU DID!", end="\033[32m")
    sys.stdout.flush()
    time.sleep(sweepTime*30)       # A dramatic pause
    cardNumber = -1   # Have to offset this value to get even rows of cards
    theRow = 3
    for card in tableSpace:
        cardNumber += 1
        if(cardNumber % 12 == 0):
            theRow += 2
            newRowPlace = "\33[" + str(theRow) + ";3H"
            print(newRowPlace, end="")
        if(card == theColor):
            theColorCount += 1
            if(card == "HEARTS"): print(f"{heartSuit}{fYel}{str(theColorCount).zfill(2)}", end=allOff)
            if(card == "SPADES"): print(f"{spadeSuit}{fYel}{str(theColorCount).zfill(2)}", end=allOff)
            if(card == "DIAMONDS"): print(f"{diamondSuit}{fYel}{str(theColorCount).zfill(2)}", end=allOff)
            if(card == "CLUBS"): print(f"{clubSuit}{fYel}{str(theColorCount).zfill(2)}", end=allOff)
        else:
            print("\33[3C", end="")
        sys.stdout.flush() # This is to force the print(). Without it, nothing is displayed until the loop is done.
        time.sleep(sweepTime) # Amount of time before the next card is shown
    print('\33[0m', end="") # Reset the escape sequences used for coloring the cards
    print("\33[25;3H",end="")
    if (theCount == quantOfTheColor): # On exact match
        print("\033[5;32mYOU COUNTED", str(theCount) + " OUT OF " + str(quantOfTheColor), theColor + "." + "\33[27;3HYOU EARNED $" + str(endScore) + " FOR THE MOB.\33[0m")
    else:
        print("\033[33mYOU COUNTED", str(theCount) + " OUT OF " + str(quantOfTheColor), theColor + "." + "\33[27;3HYOU EARNED $" + str(endScore) + " FOR THE MOB.\033[0m")
    input("\033[31m\33[30;3H" + "PRESS ENTER FOR YOUR NEXT MISSION" + "\033[0m")
    if endScore < int(missionGoal):
        failure()
    else:
        if difficulty != 9:
            missionScreen()
        else:
            win()


intro()
