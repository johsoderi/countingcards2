# CountingCards2


During the first week of my DevOps education, we got a really simple task:<br><br>
_Write a "game" in Python where a random number is generated, the user guesses what it is and the script then prints "Correct!" or "Wrong!"._ <br>

Having a bit of time left, I wanted to spice it up a bit! But I took it too far... Way too far.
<br><br>Anyway, I had forgotten all about it, but just found it again and it's actually more fun than I remember! At least the semi-graphical ascii effects might be interesting to someone.. So here it goes, for everyone to enjoy!

![/ScreenCapture.mov](/ScreenCapture.mov)
